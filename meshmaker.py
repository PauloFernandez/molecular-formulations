import bempp.api
import numpy as np

#mol_name = '1K4R_pdb2pqr_modR'
mol_name = '5pti'
mesh_den = 2
mol_directory = 'Molecule/' + mol_name
geom_directory = mol_directory + '/geometry'
mesh_directory = mol_directory + '/mesh'
mesh_name = '{}/{}_d{:04.1f}.msh'.format(mesh_directory, mol_name, mesh_den)

print 'Leyendo'

face_file = open('{}/{}_d{:04.1f}.face'.format(geom_directory, mol_name, mesh_den),'r').read()
vert_file = open('{}/{}_d{:04.1f}.vert'.format(geom_directory, mol_name, mesh_den),'r').read()

print 'Formateando'

faces = np.vstack(np.char.split(face_file.split('\n')[3:-1]))[:,:3].astype(int) - 1
verts = np.vstack(np.char.split(vert_file.split('\n')[3:-1]))[:,:3].astype(float)

print faces.shape

print verts.shape

print 'Ensamblando'
grid = bempp.api.grid_from_element_data(verts.transpose(), faces.transpose())

print 'Exportando'
bempp.api.export(grid=grid, file_name=mesh_name)

